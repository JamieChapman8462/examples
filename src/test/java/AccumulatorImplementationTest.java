import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** Tests the AccumulatorImplementation class. */
public class AccumulatorImplementationTest
{
    /** The thread pool size. */
    private static final int THREAD_POOL_SIZE = 100;

    /** The total amount of threads spawned. */
    private static final int THREAD_TOTAL = 1000;

    /** Resets the total before each method. */
    @BeforeMethod
    public void resetTotal()
    {
        new AccumulatorImplementation().reset();
    }

    /** Tests the initial value of {@link AccumulatorImplementation#TOTAL}.*/
    @Test
    public void testInitialTotalValue()
    {
        Assert.assertEquals(new AccumulatorImplementation().getTotal(), 0);
    }

    /** Tests the {@link AccumulatorImplementation#getTotal()}. */
    @Test
    public void testGetTotal()
    {
        final AccumulatorImplementation accumulatorImplementation = new AccumulatorImplementation();
        accumulatorImplementation.accumulate(1,2,3,4,5);
        Assert.assertEquals(accumulatorImplementation.getTotal(), 15);
    }

    /** Tests the {@link AccumulatorImplementation#accumulate(int...)}. */
    @Test
    public void testAccumulate() throws InterruptedException
    {
        final AccumulatorImplementation accumulatorImplementation = new AccumulatorImplementation();
        accumulatorImplementation.reset();
        Assert.assertEquals(accumulatorImplementation.getTotal(), 0);
        ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        for (int i = 0; i < THREAD_TOTAL; i++)
        {
            Runnable worker = new MyRunnable();
            executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated())
        {
            // Waits for previously submitted threads to finish execution.
        }
        Assert.assertEquals(new AccumulatorImplementation().getTotal(), 4000);
    }
}

/** Used to create multiple threads for {@link AccumulatorImplementationTest#testAccumulate}. */
class MyRunnable implements Runnable
{
    public void run()
    {
        final AccumulatorImplementation accumulatorImplementation = new AccumulatorImplementation();
        accumulatorImplementation.accumulate(2,2);
    }
}
